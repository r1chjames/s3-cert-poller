package main

import (
	"fmt"
	"os"
	"s3-cert-poller/s3Downloader"
)

func main() {

	s3Bucket := os.Getenv("S3_BUCKET")
	s3Prefix := os.Getenv("S3_PREFIX")
	downloadLocation := os.Getenv("DOWNLOAD_LOCATION")

	fmt.Println("---- Application Started ----")
	fmt.Println("Using Application Parameters:")
	fmt.Println(fmt.Sprintf("S3 Bucket:			%s", s3Bucket))
	fmt.Println(fmt.Sprintf("S3 Prefix:			%s", s3Prefix))
	fmt.Println(fmt.Sprintf("Download Location:		%s", downloadLocation))

	s3Downloader.DownloadAllFilesInS3WithPrefix(s3Bucket, s3Prefix, downloadLocation)
}
