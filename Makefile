help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: clean
clean: ## Clean working directory
	rm -f main
	go clean

.PHONY: install
install: ## Install Go dependencies
	go get

.PHONY: test
test: install## Start Kafka and Kafka Connect containers and run test hardness pack
	go test

.PHONY: build_app
build: clean install
	go build main.go

.PHONY: all ## Clean, build, tag, and then release Docker image
build: clean build