package s3Downloader

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"os"
	"strings"
)

func createS3Session() *session.Session {

	fmt.Println("Creating session")
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("eu-west-1"),
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok && aerr.Code() == "NoCredentialProviders" {
			exitErrorf("Unable to find any valid credentials. Verify you have credentials locally.")
		}
		exitErrorf("Unable to connect to AWS, %v.", err)
	}

	return sess
}

func listFilesInBucket(s3Bucket string, s3Prefix string) *s3.ListObjectsOutput {
	fmt.Println("Listing S3 bucket contents")
	s3Client := s3.New(createS3Session())

	params := &s3.ListObjectsInput{
		Bucket: aws.String(s3Bucket),
		Prefix: aws.String(s3Prefix),
	}

	resp, err := s3Client.ListObjects(params)
	if err != nil {
		exitErrorf("Unable to list S3 bucket contents, %v.", err)
	}
	return resp
}

func DownloadAllFilesInS3WithPrefix(s3Bucket string, s3Prefix string, downloadLocation string) {
	filesInBucket := listFilesInBucket(s3Bucket, s3Prefix)

	for _, key := range filesInBucket.Contents {
		fmt.Println(fmt.Sprintf("Downloading %s from S3 bucket %s", *key.Key, s3Bucket))
		s3Key := *key.Key
		fileName := s3Key[strings.LastIndex(s3Key, "/"):len(s3Key)]
		downloadFileFromS3(s3Bucket, *key.Key, fmt.Sprintf("%s%s", downloadLocation, string(fileName)))
	}
}

func downloadFileFromS3(s3Bucket string, s3Key string, downloadLocation string) {

	s3Downloader := s3manager.NewDownloader(createS3Session())

	file, err := os.Create(downloadLocation)
	if err != nil {
		fmt.Println(err)
	}

	defer file.Close()

	numBytes, err := s3Downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(s3Bucket),
			Key:    aws.String(s3Key),
		})

	if err != nil {
		exitErrorf("Unable to download item %q, %v", s3Key, err)
	}

	fmt.Println("Downloaded", file.Name(), numBytes, "bytes")
}

func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}
