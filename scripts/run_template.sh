#!/usr/bin/env bash

docker stop s3-cert-poller
docker rm s3-cert-poller
docker pull registry.gitlab.com/r1chjames/s3-cert-poller:master


docker run \
  --rm \
  --name s3-cert-poller \
  -e S3_BUCKET="$S3_BUCKET" \
  -e S3_PREFIX="$S3_PREFIX" \
  -e DOWNLOAD_LOCATION="$DOWNLOAD_LOCATION" \
  -e AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID" \
  -e AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY" \
  -v $DOWNLOAD_LOCATION:/dl \
  registry.gitlab.com/r1chjames/s3-cert-poller:master
